package handler

import (
	"context"

	"gitee.com/waagik/demo/proto/greeter"
)

type Greeter struct{}

func (s *Greeter) Hello(ctx context.Context, req *greeter.Request, resp *greeter.Response) error {
	resp.Greeting = "Hello, " + req.Name + "\n"
	return nil
}
