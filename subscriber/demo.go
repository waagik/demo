package subscriber

import (
	"context"
	log "github.com/micro/go-micro/v2/logger"

	demo "gitee.com/waagik/demo/proto/demo"
)

type Demo struct{}

func (e *Demo) Handle(ctx context.Context, msg *demo.Message) error {
	log.Info("Handler Received message: ", msg.Say)
	return nil
}

func Handler(ctx context.Context, msg *demo.Message) error {
	log.Info("Function Received message: ", msg.Say)
	return nil
}
