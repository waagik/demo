package model

import "github.com/jinzhu/gorm"

var _db DbModel

type DbModel struct {
	db *gorm.DB
}

func SetDb(db *gorm.DB) {
	_db = DbModel{db: db}
}
