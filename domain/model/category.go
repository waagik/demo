package model

import (
	"github.com/jinzhu/gorm"
)

type Category struct {
	ID                  int64  `gorm:"primary_key;not_null;auto_increment" json:"id"`
	CategoryName        string `gorm:"unique_index,not_null" json:"category_name"`
	CategoryLevel       uint32 `json:"category_level"`
	CategoryParent      int64  `json:"category_parent"`
	CategoryImage       string `json:"category_image"`
	CategoryDescription string `json:"category_description"`
}

type CategoryModel struct {
	db *gorm.DB
}

func NewCategoryModel() *CategoryModel {
	return &CategoryModel{_db.db}
}

// 根据ID删除Category2信息
func (m *CategoryModel) FindById(categoryId int64) (category Category, err error) {
	return category, m.db.First(&category, categoryId).Error
}

// 获取结果集
func (m *CategoryModel) FindAll() (categoryList []Category, err error) {
	return categoryList, m.db.Find(&categoryList).Error
}
