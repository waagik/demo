package service

import (
	"gitee.com/waagik/demo/domain/model"
)

//创建
func NewCategoryService() *CategoryService {
	return &CategoryService{model.NewCategoryModel()}
}

type CategoryService struct {
	m *model.CategoryModel
}

//查找
func (s *CategoryService) FindCategoryById(categoryId int64) (model.Category, error) {
	return s.m.FindById(categoryId)
}

//查找
func (s *CategoryService) FindAllCategory() ([]model.Category, error) {
	return s.m.FindAll()
}
