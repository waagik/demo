package common

import (
	"encoding/json"
)

//通过json tag 进行结构体赋值
func SwapTo(from, to interface{}) (err error) {
	dataByte, err := json.Marshal(from)
	if err != nil {
		return
	}
	err = json.Unmarshal(dataByte, to)
	return
}
