
GOPATH:=$(shell go env GOPATH)
MODIFY=Mproto/imports/api.proto=github.com/micro/go-micro/v2/api/proto

.PHONY: proto
proto:
	protoc --micro_out=${MODIFY}:. --go_out=${MODIFY}:. --proto_path=. proto/demo/demo.proto
	protoc --micro_out=. --go_out=. --proto_path=. ./proto/greeter/*.proto

.PHONY: build
build: proto
	go build -o demo-service *.go

.PHONY: test
test:
	go test -v ./... -cover

.PHONY: docker
docker:
	docker build . -t demo-service:latest
